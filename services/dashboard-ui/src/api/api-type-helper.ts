
import { DatasetBinaryFieldSchemaDto, DatasetReferenceFieldSchemaDto, DatasetStringFieldSchemaDto } from "./generated";

export type DatasetFieldSchema = DatasetReferenceFieldSchemaDto | DatasetStringFieldSchemaDto | DatasetBinaryFieldSchemaDto;
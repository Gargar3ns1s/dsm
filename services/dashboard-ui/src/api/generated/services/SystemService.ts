/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class SystemService {

    /**
     * run cleanup jobs
     * This call allows manually triggering cleanup.
     * Usually it is called periodically by a cron job to keep everything tidy.
     * @returns any repository with a matching schema definition already exists, returns the existing repo
     * @throws ApiError
     */
    public static systemControllerCleanup(): CancelablePromise<Record<string, any>> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/system/cleanup',
            errors: {
                500: `unknown error`,
            },
        });
    }

}

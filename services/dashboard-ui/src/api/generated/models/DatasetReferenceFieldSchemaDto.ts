/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DatasetReferenceFieldSchemaDto = {
    name: string;
    type: string;
    target: string;
};


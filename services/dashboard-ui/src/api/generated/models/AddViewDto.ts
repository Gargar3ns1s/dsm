/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ViewFieldDto } from './ViewFieldDto';

export type AddViewDto = {
    name: string;
    base: string;
    fields: Array<ViewFieldDto>;
};


/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DatasetBinaryFieldSchemaDto } from './DatasetBinaryFieldSchemaDto';
import type { DatasetReferenceFieldSchemaDto } from './DatasetReferenceFieldSchemaDto';
import type { DatasetStringFieldSchemaDto } from './DatasetStringFieldSchemaDto';

export type AddRepoDto = {
    name: string;
    schema: Array<(DatasetStringFieldSchemaDto | DatasetBinaryFieldSchemaDto | DatasetReferenceFieldSchemaDto)>;
};


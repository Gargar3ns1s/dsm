/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ViewFieldDto = {
    name: string;
    field: string;
    via?: string;
};


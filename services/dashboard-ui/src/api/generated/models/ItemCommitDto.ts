/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FieldIntegrityDto } from './FieldIntegrityDto';

export type ItemCommitDto = {
    integrity: Record<string, FieldIntegrityDto>;
};


/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type InitItemInsertDto = {
    id: string;
    /**
     * latest date for comitting the new item
     */
    deadline: string;
};


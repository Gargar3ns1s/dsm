/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DatasetBinaryFieldSchemaDto } from './DatasetBinaryFieldSchemaDto';
import type { DatasetReferenceFieldSchemaDto } from './DatasetReferenceFieldSchemaDto';
import type { DatasetStringFieldSchemaDto } from './DatasetStringFieldSchemaDto';
import type { RepoInfoDto } from './RepoInfoDto';
import type { ViewInfoDto } from './ViewInfoDto';

export type DatasetInfoDto = {
    name: string;
    createdAt: string;
    schema: Array<(DatasetStringFieldSchemaDto | DatasetBinaryFieldSchemaDto | DatasetReferenceFieldSchemaDto)>;
    repo?: RepoInfoDto;
    view?: ViewInfoDto;
    referencesTo: Array<string>;
    referencedBy: Array<string>;
};


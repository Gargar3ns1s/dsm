/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DatasetBinaryFieldSchemaDto = {
    name: string;
    type: string;
    mime: string;
    storage: string;
    length?: number;
};


/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DatasetStringFieldSchemaDto = {
    name: string;
    type: string;
    length: number;
    regex?: string;
};


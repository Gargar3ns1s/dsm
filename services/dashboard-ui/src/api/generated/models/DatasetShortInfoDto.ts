/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DatasetShortInfoDto = {
    name: string;
    createdAt: string;
};


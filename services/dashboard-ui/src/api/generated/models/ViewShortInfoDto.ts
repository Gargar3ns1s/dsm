/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ViewShortInfoDto = {
    name: string;
    createdAt: string;
    base: string;
};


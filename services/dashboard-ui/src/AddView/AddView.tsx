
import { ApplyYamlResource } from "../AddRepo/AddRepo";
import { ViewService } from "../api/generated/services/ViewService";


export function AddView() {
  return (
    <div>
      <h1>Add View</h1>
      <p><i>Enter YAML view description</i></p>
      <ApplyYamlResource apiCallback={ViewService.viewControllerAdd} />
    </div>
  );
}

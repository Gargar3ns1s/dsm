
export interface Config {
  basename: string;
  apiRoot: string;
}

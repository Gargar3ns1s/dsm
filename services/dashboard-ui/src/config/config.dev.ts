
import { Config } from "./typing";


export const config: Config = {
  basename: '/',
  apiRoot: '/api',
}

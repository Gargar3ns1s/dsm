
export type { Config } from './typing';
// NOTE: config file will get replaced with environment specific config during docker build
export { config } from './config';

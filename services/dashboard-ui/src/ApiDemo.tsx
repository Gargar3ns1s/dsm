
import React from 'react';


async function runRequest(endpoint: string, method: string, data?: Object): Promise<Object> {
  console.log('>> request data:', data)

  let body
  const headers: {[key: string]: string} = {}

  if (data && data instanceof FormData) {
    body = data
  } else if (data instanceof Object) {
    body = JSON.stringify(data)
    headers['Content-Type'] = 'application/json'
  }

  const resp = await fetch(endpoint, { method, headers, body })
  const respData = await resp.json()
  if (resp.status >= 200 && resp.status < 300) {
    console.log('<< reponse data:', respData)
    return respData;
  } else {
    throw respData
  }
}

export class ApiDemo extends React.Component {

  async dsmCreateDataset(): Promise<void> {  // TODO: type
    const schema = {
      name: 'foo01',
      schema: [
        {
          name: 'name',
          type: 'string',
          length: 64
        },
        {
          name: 'comment',
          type: 'string'
        },
        {
          name: 'tel',
          type: 'string',
          length: 64,
          regex: '/^\\+?\\d+ \\d+$/i'
        }
      ]
    }
    await runRequest('/api/repo', 'POST', schema)
  }

  async dsmCreateDataset2(): Promise<void> {  // TODO: type
    const schema = {
      name: 'foo02',
      schema: [
        {
          name: 'familyName',
          type: 'string',
          length: 64
        },
        {
          name: 'foo01_ref',
          type: 'reference',
          target: 'foo01',
        },
        {
          name: 'image',
          type: 'binary',
          mime: 'image/jpeg',
          length: 2**20,  // max 1MB
          storage: 'default',
        },
        {
          name: 'image_localdisk',
          type: 'binary',
          mime: 'image/jpeg',
          length: 2**20,  // max 1MB
          storage: 'localdisk',
        },
      ]
    }
    await runRequest('/api/repo', 'POST', schema)
  }

  async dsmDeleteDataset(datasetId: string): Promise<void> { // TODO: type
    await runRequest(`/api/repo/${datasetId}`, 'DELETE')
  }

  async dsmAddItem(): Promise<void> {
    const datasetId = 'foo01'

    const item = new FormData()
    item.append('name', 'blub')
    item.append('comment', 'this is a comment')
    item.append('tel', '+49 12345')

    await runRequest(`/api/repo/${datasetId}/item`, 'POST', item)
  }

  async dsmAddItem2(): Promise<void> {
    const datasetId = 'foo02'

    const item = new FormData()
    item.append('familyName', 'Blubberson')
    item.append('foo01_ref', '2')
    item.append('image', await fetch('/demo.jpg').then(r => r.blob()), 'my_file_name');
    item.append('image_localdisk', await fetch('/demo.jpg').then(r => r.blob()), 'my_file_name');

    await runRequest(`/api/repo/${datasetId}/item`, 'POST', item)
  }

  async dsmCreateView(): Promise<void> {
    const view = {
      name: 'foo-viewA',
      base: 'foo02',
      fields: [
        { name: 'name', via: 'foo01_ref' },
        { name: 'tel', via: 'foo01_ref' },
        { name: 'last_name', field: 'familyName', },
      ],
      filter: [[
        {
          field: 'familyName',
          op: '=',
          value: 'blub'
        },
      ]]
    }
    await runRequest('/api/view', 'POST', view)
  }

  async dsmDeleteView(datasetId: string): Promise<void> { // TODO: type
    await runRequest(`/api/view/${datasetId}`, 'DELETE')
  }

  async dsmCreateView2(): Promise<void> {
    const view = {
      name: 'nonref_view',
      base: 'foo01',
      fields: [
        { name: 'name' },
        { name: 'tel' },
      ],
      filter: [[
        {
          op: 'noref',
          dataset: 'foo02',
          field: 'foo01_ref',
        },
      ]]
    }
    await runRequest('/api/view', 'POST', view)
  }

  render() {
    return (
      <div>
        <button onClick={() => this.dsmCreateDataset()}>create dataset</button>
        <button onClick={() => this.dsmCreateDataset2()}>create dataset 2</button>
        <button onClick={() => this.dsmDeleteDataset('foo01')}>delete dataset</button>
        <button onClick={() => this.dsmDeleteDataset('foo02')}>delete dataset 2</button>
        <button onClick={() => this.dsmAddItem()}>add item</button>
        <button onClick={() => this.dsmAddItem2()}>add item 2</button>
        <br/>
        <button onClick={() => this.dsmCreateView()}>create view</button>
        <button onClick={() => this.dsmCreateView2()}>create view 2</button>
        <button onClick={() => this.dsmDeleteView('foo-viewA')}>delete view</button>
        <button onClick={() => this.dsmDeleteView('nonref_view')}>delete view 2</button>
      </div>
    );
  }
}


import styles from './NoPage.module.scss';


export function NoPage() {
  return (
    <div className={styles.wrapper}>
      <h1>404 - Page not found</h1>
    </div>
  );
}

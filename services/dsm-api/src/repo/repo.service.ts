
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DatasetBinaryFieldSchema, DatasetModel, DatasetReferenceFieldSchema } from 'src/_models';
import { DatabaseService } from 'src/database.service';
import { DatasetService } from 'src/dataset/dataset.service';
import { buildTable } from './build_table';
import { StorageBackend, getStorageBackend } from 'src/_storage_backends';
import { getConfig } from 'src/config';
import { Knex } from 'knex';
import { PageQueryDto } from 'src/common.dto';
import { DatasetShortInfoDto } from 'src/dataset/dataset.dto';
import { getPageQuery } from 'src/controller_utils';


@Injectable()
export class RepoService {
  private _config = getConfig();

  constructor(protected readonly db: DatabaseService,
              protected readonly datasets: DatasetService) {}

  async getRepo(datasetId: string,
                options: {
                  transaction?: Knex.Transaction,
                } = {}
               ): Promise<DatasetModel | null> {
    this.datasets.checkDatasetId(datasetId)
    const dbClient = options.transaction || this.db.client;

    // TODO: cache DatasetModel instances
    const dbResult = await dbClient
      .table('_dsm_repos')
      .join('_dsm_datasets', '_dsm_repos.name', '=', '_dsm_datasets.name')
      .where({
        '_dsm_datasets.name': datasetId,
        '_dsm_datasets.created': true,
        '_dsm_datasets.deletedAt': null,
      })
      .select('*')
      .limit(1);

    if (!dbResult || !dbResult.length) {
      return null;
    }
    return DatasetModel.fromDb(dbResult[0]);
  }

  async isRepo( datasetId: string,
                options: {
                  transaction?: Knex.Transaction,
                } = {}): Promise<boolean> {
    this.datasets.checkDatasetId(datasetId)
    const dbClient = options.transaction || this.db.client;

    const dbResult = await dbClient
      .table('_dsm_repos')
      .select('name')
      .where({ name: datasetId })
      .limit(1);
    return !!dbResult && !!dbResult.length
  }

  getStorage(fieldSchema: DatasetBinaryFieldSchema): StorageBackend {
    const storageConfig = this._config.storageBackends[fieldSchema.storage];
    const storageCls = getStorageBackend(storageConfig.type);
    return new storageCls(storageConfig);
  }

  async listMinimalInfo(pagination: PageQueryDto): Promise<DatasetShortInfoDto[]> {
    const pageQuery = getPageQuery(pagination, 1000);
    return await this.db.client
      .table('_dsm_repos')
      .join('_dsm_datasets', '_dsm_repos.name', '=', '_dsm_datasets.name')
      .select('_dsm_repos.name', '_dsm_datasets.createdAt')
      // filter out deleted & creating repos
      .where({
        created: true,
        deletedAt: null,
      })
      .offset(pageQuery.offset).limit(pageQuery.limit);
  }

  /**
   * If the call fails, the repo might be partially created and in a dangling state (not visible or usable).
   *  In this case creation can be retried (transient errors) - once the request for the same create was
   *   successful, the error is resolved and the repo can be used
   *  A partially cretated repo can also be deleted, which clean up all partial resources
   *
   * Any references that the repo has to other repos are effective once creation started even if repo
   *  creation fails and is left in a dangling state. This e.g. causes datasets that are referenced
   *  by a dangling repo to be blocked from deletion
   */
  async addRepo(newRepo: DatasetModel): Promise<{ created: boolean, dataset: DatasetModel }> {
    // write repo metadata
    const dbDataset = await this.db.client.transaction(async (transaction) => {
      // check if dataset already exists
      const existingDataset = await this.datasets.getDataset(newRepo.name, { transaction, includeCreating: true, includeDeleting: true });
      if (existingDataset) {
        // fail if dataset is deleting
        if (existingDataset.deletedAt)
          throw new HttpException('dataset name still occupied by dataset that is currently getting deleted', HttpStatus.CONFLICT)
        // check if existing dataset is repo
        if (await this.isRepo(newRepo.name, { transaction })) {
          // check if schemas match
          if (this.datasets.schemaEqual(newRepo.schema, existingDataset.schema)) {
            return existingDataset;
          } else {
            throw new HttpException('repo with the same name exists but with a different schema', HttpStatus.CONFLICT)
          }
        } else {
          throw new HttpException('dataset with the same name already exists', HttpStatus.CONFLICT)
        }
      } else {
        // check if binary fields use valid (configured) storage
        for (const schema of newRepo.schema.filter((v) => v instanceof DatasetBinaryFieldSchema)) {
          try {
            const storage = this.getStorage(<DatasetBinaryFieldSchema>schema)
            if (!storage)
              throw new Error();
          } catch {
            throw new HttpException(`could not get storage backend for field "${schema.name}"`, HttpStatus.BAD_REQUEST);
          }
        }
        // write new metdata, dataset will by default be in dangling state
        await transaction.table('_dsm_datasets').insert(newRepo.toDb());
        await transaction.table('_dsm_repos').insert({name: newRepo.name});
        const refs = newRepo.schema
          .filter((f) => (f instanceof DatasetReferenceFieldSchema))
          .map((f: DatasetReferenceFieldSchema) => ({ origin: newRepo.name, target: f.target }));
        if (refs.length)
          await transaction.table('_dsm_refs').insert(refs);
          return await this.datasets.getDataset(newRepo.name, { transaction, includeCreating: true });
      }
    });

    // return success if repo in already makred created
    if (dbDataset.created)
      return { dataset: dbDataset, created: false }

    // create table for repo
    await this.db.client.transaction(async (transaction) => {
      if (await transaction.schema.hasTable(dbDataset.name))
        return;
      // NOTE: CREATE TABLE implicitly commits the transaction
      await transaction.schema.createTable(
        dbDataset.name,
        (builder) => buildTable(dbDataset.schema, builder, transaction));
    });

    // init storage
    for (const schema of dbDataset.schema.filter((v) => v instanceof DatasetBinaryFieldSchema)) {
      const storage = this.getStorage(<DatasetBinaryFieldSchema>schema)
      await storage.initStorage(dbDataset.name, <DatasetBinaryFieldSchema>schema)
    }

    // mark repo as fully created
    await this.db.client
      .table('_dsm_datasets')
      .update({ created: true })
      .where('name', dbDataset.name);

    return { dataset: dbDataset, created: true }
  }

  async deleteRepo(datasetId: string): Promise<void> {
    const dataset = await this.db.client.transaction(async (transaction) => {
      // make sure deltion is allowed
      await this.datasets.ensureDatasetDeletable(datasetId, { transaction });
      // make sure re're really dealing with a repo
      if (!(await this.isRepo(datasetId, { transaction })))
        throw new HttpException(`dataset ${datasetId} is not a repo`, HttpStatus.BAD_REQUEST);

      // mark repo as deletion pending
      await transaction
        .table('_dsm_datasets')
        .where({
          name: datasetId,
          deletedAt: null,  // don't update if deletedAt is already set
        })
        .update({ deletedAt: transaction.fn.now() });

      // unregister dataset references
      await transaction
        .table('_dsm_refs')
        .where({ origin: datasetId }).orWhere({ target: datasetId })
        .delete();

      return this.datasets.getDataset(datasetId, { transaction, includeCreating: true, includeDeleting: true });
    });

    // clean up storage
    const fieldsWithBinaryData = new Set(<DatasetBinaryFieldSchema[]>dataset.schema.filter((f) => f.type === DatasetBinaryFieldSchema.type));
    await Promise.all([...fieldsWithBinaryData].map(async(fieldSchema) => {
      const storage = this.getStorage(fieldSchema);
      storage.removeDataset(dataset.name);
    }));

    // delete repo table
    await this.db.client.schema.dropTableIfExists(dataset.name);

    // delete metadata entry
    // NOTE: deletes entry in _dsm_repos through foreign key
    await this.db.client
      .table('_dsm_datasets')
      .where({ name: dataset.name })
      .delete();
  }
}

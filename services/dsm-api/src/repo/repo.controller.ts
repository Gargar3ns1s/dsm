
import { Body, Controller, Get, HttpStatus, Post, Query, Res, Delete, Param, UseInterceptors, UploadedFiles, Req, RawBodyRequest } from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { wrapHttpErrorAsyncFn } from '../controller_utils';
import { AddRepoDto, InitItemInsertDto, ItemCommitDto } from './repo.dto';
import { DatabaseService } from '../database.service';
import { DatasetModel } from '../_models';
import { Response } from 'express';
import { DatasetItemDto, DatasetShortInfoDto } from 'src/dataset/dataset.dto';
import { NamedResourceDto, PageQueryDto } from 'src/common.dto';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { RepoService } from './repo.service';
import { ItemInsertService } from './item_insert.service';
import { Request } from 'express';
import { DatasetItemService } from 'src/dataset/dataset_item.service';


@Controller('/repo')
@ApiTags('repo')
export class RepoController {
  constructor(protected readonly db: DatabaseService,
              protected readonly repos: RepoService,
              protected readonly datasetItems: DatasetItemService,
              protected readonly itemInsert: ItemInsertService,
             ) {}

  @Get('/')
  @ApiOperation({
    summary: 'list repositories',
    description: [
      'List all available and ready repositories with minimal info.',
      'Repos that are __not__ in a clean state (not completely created; partially deleted) are excluded',
    ].join('  \n'),
  })
  @ApiResponse({
    status: 200,
    description: 'get minimal info for all available repositories',
    type: [ DatasetShortInfoDto ],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async listIds(@Query() pagination: PageQueryDto): Promise<DatasetShortInfoDto[]> {
    return wrapHttpErrorAsyncFn(async() => this.repos.listMinimalInfo(pagination));
  }

  @Post('/')
  @ApiOperation({
    summary: 'create repository',
    description: [
      'Create a new repository',
      'This endpoint can be called multiple times with the same repo name, as long as the schema matches that of an \
        existing repo under this name. This allows for simple "create if not present" logic.',
      'If the call fails, resources may be partially created. In that case the repo is marked as not ready and will \
        __not__ be available for item operations. This can either be resolved by retrying the create (this) call until \
        success or by deleting the (partially created) repo.',
      'Note that a partially created repo still occupies it\'s name and thus creating a different dataset under the \
        same name will fail.'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'new repository has been created',
    type: DatasetShortInfoDto,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'repository with a matching schema definition already exists, returns the existing repo',
    type: DatasetShortInfoDto,
  })
  @ApiResponse({
    status: HttpStatus.CONFLICT,
    description: 'repository with the same name exists but the schema differs to the one defined in this request, return the conflicting repo',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'got incorrect data in request',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async add(@Body() data: AddRepoDto, @Res() res: Response): Promise<void> {
    return await wrapHttpErrorAsyncFn(async() => {
      const newDataset = new DatasetModel(data);
      const {created, dataset} = await this.repos.addRepo(newDataset);
      res.status(created ? HttpStatus.CREATED : HttpStatus.OK);
      res.send({ name: dataset.name, createdAt: dataset.createdAt })
    });
  }

  @Delete('/:datasetId')
  @ApiOperation({
    summary: 'delete repository',
    description: [
      'Delete a repository and free all associated resources',
      'sematics are similar to *create repository*: if deletion fails we might end up with a partially deleted \
        repo. This endpoint can be called again to eventually finish the deletion process. This is especiall \
        important since cleaning up large amounts of storage might take some time and produce multiple timeouts \
        before all files have been removed.',
      'In the meantime it is ensured that the repo can not be used while deleting (references are removed at the \
        the start of the deletion process and a repository that is in a deleting state is invisible to item \
        operations)'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'deletion successful',
    type: NamedResourceDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'repo not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async delete(@Param('datasetId') datasetId: string): Promise<NamedResourceDto> {
    return await wrapHttpErrorAsyncFn(async() => {
      await this.repos.deleteRepo(datasetId);
      return { name: datasetId }
    });
  }

  @Post('/:datasetId/item')
  @ApiOperation({
    summary: 'init item insert',
    description: [
      'Initializes a new item insert operation.',
      'Inserting an item requires the following steps:',
      '  1. initialize insert to obtain a new dataset item id',
      '  2. use the id to upload data for the new dataset item',
      '  3. commit the new item',
      '',
      'The new item will only be available for reading once the commit was successful. The new item needs to \
      be comitted before it\'s *deadline* exceeds, otherwise the insert is considered unsuccessful and all data \
      associated with the item will get cleaned up.'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'new insert transaction has been initialized',
    type: InitItemInsertDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'repo not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async initItemInsert(@Param('datasetId') datasetId: string): Promise<InitItemInsertDto> {
    return wrapHttpErrorAsyncFn(async() => this.itemInsert.initInsert(datasetId));
  }

  @Post('/:datasetId/item/:itemId')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      additionalProperties: {
        oneOf: [
          { type: 'string' },
          { type: 'string', format: 'binary' },
        ]
      }
    },
  })
  @ApiOperation({
    summary: 'upload item fields',
    description: [
      'Use this to upload multiple fields of item data at once',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'upload successful',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'repo or item not found or not available for upload (insert not initialized or deadline exceeded)',
  })
  @ApiResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'data does not conform to schema',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  @UseInterceptors(AnyFilesInterceptor())
  async uploadItem( @Param('datasetId') datasetId: string,
                    @Param('itemId') itemId: string,
                    @Body() data: { [key: string]: string },
                    @UploadedFiles() files: Express.Multer.File[],
                  ): Promise<{}> {
    return wrapHttpErrorAsyncFn(async() => {
      const filesByField = {}
      for (const file of files)
        filesByField[file.fieldname] = file;

      await this.itemInsert.uploadFields(datasetId, itemId, {...data, ...filesByField});
      return {};
    });
  }

  @Post('/:datasetId/item/:itemId/field/:fieldName')
  @ApiConsumes('text/plain', 'application/octet-stream', 'image/jpeg', '*/*')
  @ApiBody({
    schema: {
      oneOf: [
        { type: 'string' },
        { type: 'string', format: 'binary' }
      ]
    },
  })
  @ApiOperation({
    summary: 'upload data for a single field',
    description: [
      'Use this to upload data for a single field in a datapoint',
      'Note that the mime type in the request needs to be set according to the schema',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'upload successful',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'repo or item not found (similar to upload) or field not in schema',
  })
  @ApiResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'data does not conform to schema',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async uploadItemField(@Param('datasetId') datasetId: string,
                        @Param('itemId') itemId: string,
                        @Param('fieldName') fieldName: string,
                        @Req() req: RawBodyRequest<Request>
                       ): Promise<{}> {
    return wrapHttpErrorAsyncFn(async() => {
      await this.itemInsert.uploadSingleField(datasetId, itemId, fieldName, req, req.header('content-type'));
      return {};
    });
  }

  @Post('/:datasetId/item/:itemId/commit')
  @ApiOperation({
    summary: 'commit new item to the dataset',
    description: [
      'Validates all data that was uploaded or the item. Every field is checked for integrity against a hash value \
        provided in the query body.',
      'If the item is complete and integrity is valid the item will be marked as committed and will not be sealed \
        against any alternation.'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'commit successful; new item is now available',
    type: DatasetItemDto,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'item was already committed before, integrity is not checked again',
    type: DatasetItemDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'repo or item not found',
  })
  @ApiResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'data does not conform to schema',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async commitItem(@Param('datasetId') datasetId: string,
                   @Param('itemId') itemId: string,
                   @Body() data: ItemCommitDto,
                   @Res() res: Response,
                  ): Promise<void> {
    return wrapHttpErrorAsyncFn(async() => {
      const created = await this.itemInsert.commit(datasetId, itemId, data);
      res.status(created ? HttpStatus.CREATED : HttpStatus.OK);
      res.send(await this.datasetItems.getItem(datasetId, itemId));
    });
  }
}

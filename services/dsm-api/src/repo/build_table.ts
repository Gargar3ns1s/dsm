
import { Knex } from "knex";
import { DatasetBinaryFieldSchema, DatasetFieldSchema, DatasetReferenceFieldSchema, DatasetSchema, DatasetStringFieldSchema } from "../_models";


type BuildFieldFunction<S extends DatasetFieldSchema> = (S, builder: Knex.CreateTableBuilder) => void


function buildStringField(schema: DatasetStringFieldSchema, builder: Knex.CreateTableBuilder): void {
  builder.string(schema.name, schema.length)
}

function buildReferenceField(schema: DatasetReferenceFieldSchema, builder: Knex.CreateTableBuilder): void {
  builder.integer(schema.name).unsigned().notNullable()
  builder.foreign(schema.name).references(`${schema.target}._dsm_id`).onDelete('RESTRICT')
}

function buildBinaryField(schema: DatasetBinaryFieldSchema, builder: Knex.CreateTableBuilder): void {
  builder.binary(schema.name, 32);
}


const tableBuilderFunctions: {[key: string]: BuildFieldFunction<any>} = {
  'string': buildStringField,
  'reference': buildReferenceField,
  'binary': buildBinaryField,
}

export function buildTable(schema: DatasetSchema, builder: Knex.CreateTableBuilder, knex: Knex): void {
  // primary key column, used to identify items in the dataset
  builder.increments('_dsm_id', { primaryKey: true })
  // indicates whether the item was committed
  builder.boolean('_dsm_committed').defaultTo(false)
  // record commit time
  // before the commit is done it is set to the time when the insert was initialized to track the commit deadline
  // after the commit this records the time of the commit
  builder.datetime('_dsm_committedAt').defaultTo(knex.fn.now())
  for (const field of schema)
    tableBuilderFunctions[field.type](field, builder)
}


import { ApiExtraModels, ApiProperty, getSchemaPath } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDate, IsHash, IsString, Length, ValidateNested } from "class-validator";
import { DatasetBinaryFieldSchema, DatasetReferenceFieldSchema, DatasetStringFieldSchema } from "src/_models";
import { NamedResourceDto } from "src/common.dto";
import { DatasetBinaryFieldSchemaDto, DatasetFieldSchemaBaseDto, DatasetFieldSchemaDto, DatasetReferenceFieldSchemaDto, DatasetStringFieldSchemaDto, datasetSchemaApiPropertyConfig } from "src/dataset/dataset.dto";


export class AddRepoDto extends NamedResourceDto {
  @ApiProperty(datasetSchemaApiPropertyConfig)
  @ValidateNested({ each: true })
  @Type(() => DatasetFieldSchemaBaseDto, {
    discriminator: {
      property: 'type',
      subTypes: [
        { name: DatasetStringFieldSchema.type, value: DatasetStringFieldSchemaDto},
        { name: DatasetBinaryFieldSchema.type, value: DatasetBinaryFieldSchemaDto},
        { name: DatasetReferenceFieldSchema.type, value: DatasetReferenceFieldSchemaDto},
      ],
    },
  })
  schema: DatasetFieldSchemaDto[];
}


export class InitItemInsertDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  id: string;

  @ApiProperty({ description: 'latest date for comitting the new item' })
  @IsDate()
  deadline: Date
}

export class FieldIntegrityDto {
  @ApiProperty({ description: 'sha256 hex digest' })
  @IsHash('sha256')
  sha256: string
}

@ApiExtraModels(FieldIntegrityDto)
export class ItemCommitDto {
  @ApiProperty({
    type: 'object',
    additionalProperties: { $ref: getSchemaPath(FieldIntegrityDto) }})
  integrity: { [key: string]: FieldIntegrityDto };
}

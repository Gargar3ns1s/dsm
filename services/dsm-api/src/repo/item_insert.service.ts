
import { ConflictException, Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { DatabaseService } from 'src/database.service';
import { InitItemInsertDto, ItemCommitDto } from './repo.dto';
import { RepoService } from './repo.service';
import { getConfig } from 'src/config';
import { sanitize } from './sanitize';
import { DatasetBinaryFieldSchema, DatasetReferenceFieldSchema, DatasetStringFieldSchema } from 'src/_models';
import { Readable } from 'stream';
import { Knex } from 'knex';
import { DatasetService } from 'src/dataset/dataset.service';
import { createHash } from 'crypto';


async function stream2buffer(stream: Readable): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const buffer = [];
    stream.on('data', (chunk) => buffer.push(chunk));
    stream.on('end', () => resolve(Buffer.concat(buffer)));
    stream.on('error', reject);
  });
}

@Injectable()
export class ItemInsertService {
  private _config = getConfig();

  constructor(protected readonly db: DatabaseService,
              protected readonly datasets: DatasetService,
              protected readonly repos: RepoService) {}

  async initInsert(datasetId: string): Promise<InitItemInsertDto> {
    return await this.db.client.transaction(async (transaction) => {
      if (!(await this.repos.isRepo(datasetId, { transaction })))
        throw new NotFoundException('repo not found or dataset is not a repo');
      // create new item in database
      const newItemIds = await transaction.table(datasetId).insert({});
      if (newItemIds.length !== 1)
        throw new Error(`dataset insert call returned ${newItemIds.length} values, expected exactly 1`);
      // read item to get commit deadline as repotred by the database
      const newItem = await transaction
        .table(datasetId)
        .select('_dsm_id', '_dsm_committedAt')
        .where({ '_dsm_id': newItemIds[0] })
        .limit(1);
      if (newItem.length !== 1)
        throw new Error(`failed to receive new item entry from database`);
      return {
        id: newItem[0]['_dsm_id'],
        deadline: new Date(new Date(newItem[0]['_dsm_committedAt']).valueOf() + this._config.commitDeadline),
      }
    });
  }

  async commit(datasetId: string, itemId: string, data: ItemCommitDto): Promise<boolean> {
    const repo = await this.repos.getRepo(datasetId);
    if (!repo)
      throw new NotFoundException('repo not found or dataset is not a repo');

    return await this.db.client.transaction(async (transaction) => {
      // retrieve uncommited datapoint
      const dbResult = await transaction
        .table(repo.name)
        .where('_dsm_id', itemId)
        .where('_dsm_committedAt', '<', new Date(Date.now() + this._config.commitDeadline))
        .select('*')
      if (!dbResult.length || !dbResult[0])
        throw new NotFoundException('item not found or deadline exceeded');
      const rawItem = dbResult[0]

      // return immediately if item was committed before
      if (rawItem['_dsm_committed'])
        return false;

      // check integrity
      for (const fieldSchema of repo.schema) {
        if (!(fieldSchema.name in data.integrity))
          throw new UnprocessableEntityException(`missing integrity for field "${fieldSchema.name}"`);

        if (fieldSchema instanceof DatasetStringFieldSchema ||
            fieldSchema instanceof DatasetReferenceFieldSchema) {
          const hash = createHash('sha256');
          hash.update(rawItem[fieldSchema.name], 'utf-8');
          if (hash.digest('hex').toString() !== data.integrity[fieldSchema.name].sha256)
            throw new ConflictException(`integrity check failed for field "${fieldSchema.name}"`);
        } else if (fieldSchema instanceof DatasetBinaryFieldSchema) {
          if (rawItem[fieldSchema.name].toString('hex') !== data.integrity[fieldSchema.name].sha256)
          throw new ConflictException(`integrity check failed for field "${fieldSchema.name}"`);
        } else {
          throw new Error(`integrity check not implemented for field type "${fieldSchema.type}"`)
        }
      }

      // mark item as committed
      await transaction
        .table(repo.name)
        .where('_dsm_id', itemId)
        .update({
          '_dsm_committed': true,
          '_dsm_committedAt': transaction.fn.now(),
        });
      return true;
    });
  }

  async uploadFields( datasetId: string,
                      itemId: string,
                      data: { [key: string]: string|Express.Multer.File }
                    ): Promise<void> {
    const repo = await this.repos.getRepo(datasetId);
    if (!repo)
      throw new NotFoundException('repo not found or dataset is not a repo');

    // make sure data conforms to dataset schema
    const sanitized = sanitize(repo.schema, data, { allowPartial: true });

    return await this.db.client.transaction(async (transaction) => {
      // check item commit status and deadline
      const dbItem = await this._getCommittableItem(datasetId, itemId, { transaction });
      if (!dbItem)
        throw new NotFoundException('item not found or not writable');

      // upload data to binary storage
      await Promise.all(repo.schema
        .filter((f) => f instanceof DatasetBinaryFieldSchema)
        .filter((f) => !!sanitized[f.name])
        .map(async(fieldSchema: DatasetBinaryFieldSchema) => {
          const storage = this.repos.getStorage(fieldSchema);
          await storage.write(fieldSchema, repo.name, itemId,
                              Readable.from((<Express.Multer.File>data[fieldSchema.name]).buffer));
        }));

      // update fields in the database
      await transaction
        .table(datasetId)
        .where('_dsm_id', dbItem._dsm_id)
        .update(sanitized)
    });
  }

  async uploadSingleField(datasetId: string,
                          itemId: string,
                          fieldName: string,
                          data: Readable,
                          contentType: string,
                         ): Promise<void> {
    const repo = await this.repos.getRepo(datasetId);
    if (!repo)
      throw new NotFoundException('repo not found or dataset is not a repo');
    const fieldSchema = repo.schema.find((f) => f.name === fieldName);
    if (!fieldSchema)
      throw new NotFoundException(`field "${fieldName}" is not in schema for repo "${repo.name}"`);

    const buffer = await stream2buffer(data);
    const parsed = contentType === 'text/plain' ?
      buffer.toString('utf-8') :
      { buffer, size: buffer.byteLength, mimetype: contentType };
    // make sure data conforms to dataset schema
    const sanitized = sanitize(repo.schema, { [fieldName]: parsed }, { allowPartial: true });

    return await this.db.client.transaction(async (transaction) => {
      // check item commit status and deadline
      const dbItem = await this._getCommittableItem(datasetId, itemId, { transaction });
      if (!dbItem)
        throw new NotFoundException('item not found or not writable');
      // upload data to binary storage
      if (fieldSchema instanceof DatasetBinaryFieldSchema) {
        const storage = this.repos.getStorage(fieldSchema);
        await storage.write(fieldSchema, repo.name, itemId, Readable.from(buffer));
      }
      // update fields in the database
      await transaction
        .table(datasetId)
        .where('_dsm_id', itemId)
        .update(sanitized);
    });
  }

  /**
   * returns the item id only if it can be written to (initialized but not comitted)
   */
  protected async _getCommittableItem( datasetId: string,  // assumed to be a repo; not checked!
                                      itemId: string,
                                      options: {
                                        transaction?: Knex.Transaction,
                                      } = {}): Promise<{ _dsm_id: string } | null> {
    this.datasets.checkDatasetId(datasetId)
    const dbClient = options.transaction || this.db.client;

    const dbResult = await dbClient
      .table(datasetId)
      .where('_dsm_id', itemId)
      .where('_dsm_committed', false)
      .where('_dsm_committedAt', '<', new Date(Date.now() + this._config.commitDeadline))
      .select('_dsm_id')

    if (!dbResult.length || !dbResult[0])
      return null

    return dbResult[0]
  }
}

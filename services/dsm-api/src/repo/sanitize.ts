
import { HttpException, HttpStatus } from "@nestjs/common";
import { DatasetBinaryFieldSchema, DatasetFieldSchema, DatasetReferenceFieldSchema, DatasetSchema, DatasetStringFieldSchema } from "../_models";
import { createHash } from 'crypto';


type SanitizeFunction<S extends DatasetFieldSchema, D> = (S, any) => D

function sanitizeStringField(schema: DatasetStringFieldSchema, data: any): string {
  if (typeof data !== 'string')
    throw new HttpException(
      `data for field "${schema.name}" is expected to be a string`,
      HttpStatus.UNPROCESSABLE_ENTITY)
  if (schema.length !== undefined) {
    if (data.length > schema.length)
      throw new HttpException(
        `string too long for field "${schema.name}", max ${schema.length} characters allowed`,
        HttpStatus.UNPROCESSABLE_ENTITY)
  }
  if (schema.regex !== undefined) {
    if (!schema.regex.test(data))
      throw new HttpException(
        `regex test failed for field "${schema.name}"`,
        HttpStatus.UNPROCESSABLE_ENTITY)
  }
  return data
}

function sanitizeReferenceField(schema: DatasetReferenceFieldSchema, data: any): string {
  try {
    return `${parseInt(data)}`
  } catch(err) {
    throw new HttpException(
      `data for field "${schema.name}" is expected to be an interger (refernce id)`,
      HttpStatus.UNPROCESSABLE_ENTITY)
  }
}

function sanitizeBinaryField(schema: DatasetBinaryFieldSchema, data: any): Buffer {
  // general type check
  if (typeof(data) !== 'object' || !data.buffer || !(data.buffer instanceof Buffer)) {
    throw new HttpException(
      `data for field "${schema.name}" must be a file upload`,
      HttpStatus.UNPROCESSABLE_ENTITY)
  }
  // check mime type
  if (data.mimetype !== schema.mime) {
    throw new HttpException(
      `file for field "${schema.name}" has incorrect mime type, expected ${schema.mime}`,
      HttpStatus.UNPROCESSABLE_ENTITY)
  }
  // check upload size
  if (schema.length && (data.size > schema.length || data.buffer.byteLength > schema.length)) {
    throw new HttpException(
      `file for field "${schema.name}" is too large, max size [bytes] is ${schema.length}`,
      HttpStatus.UNPROCESSABLE_ENTITY)
  }

  // TODO: calculate hash async, binary data may be quite large
  const hash = createHash('sha256');
  hash.update(<Buffer>data.buffer);
  return hash.digest()
}


const fieldSanitizerFunctions: {[key: string]: SanitizeFunction<any, any>} = {
  'string': sanitizeStringField,
  'reference': sanitizeReferenceField,
  'binary': sanitizeBinaryField,
}

export function sanitize( schema: DatasetSchema,
                          data: Object,
                          options: {
                            // allow data to omit fields from the schema
                            allowPartial?: boolean
                          } = {}
                        ): Object {
  const res = {}
  for (const fieldSchema of schema) {
    if (!(fieldSchema.name in data)) {
      if (options.allowPartial) {
        continue;
      } else {
        throw new HttpException(`missing field ${fieldSchema.name} in data`, HttpStatus.UNPROCESSABLE_ENTITY);
      }
    }
    res[fieldSchema.name] = fieldSanitizerFunctions[fieldSchema.type](fieldSchema, data[fieldSchema.name])
  }
  return res;
}

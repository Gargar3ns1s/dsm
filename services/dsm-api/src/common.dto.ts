
import { ApiProperty } from "@nestjs/swagger"
import { Transform } from "class-transformer"
import { IsInt, IsOptional, IsString, Length, Matches, Min } from "class-validator"


export const identifierRegex = /^(?!_dsm_)[A-Za-z0-9-_~]+$/i;

export class NamedResourceDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  @Matches(identifierRegex)
  name: string;
}


export class PageQueryDto {
  @ApiProperty({ required: false, example: 0 })
  @Transform(({ value }) => value === undefined ? undefined : parseInt(value))
  @IsOptional()
  @IsInt()
  @Min(0)
  page: number = 0

  @ApiProperty({ required: false, example: 10 })
  @Transform(({ value }) => value === undefined ? undefined : parseInt(value))
  @IsOptional()
  @IsInt()
  @Min(1)
  pageSize: number = Number.MAX_SAFE_INTEGER
}


import { DatasetBinaryFieldSchema } from "../_models";
import { StorageBackendConfig } from "../config";
import { Readable, Stream } from "stream";


export class StorageBackend {
  static readonly type: string; // set in subclasses
  get type(): string {
    const type = (<typeof StorageBackend>this.constructor).type
    if (!type)
      throw Error('field type not implemented')
    return type
  }

  protected readonly config: StorageBackendConfig;

  /**
   * indicates whether getUrl can be used
   */
  get hasUrl(): boolean { return false; }

  constructor(config: StorageBackendConfig) {
    if (config.type !== this.type) {
      throw Error(`got incompatible config for storage backend of type "${this.type}"`);
    }
    this.config = config;
  }

  async write(fieldSchema: DatasetBinaryFieldSchema, datasetId: string, itemId: string, data: Stream): Promise<void> {
    throw Error('not implemented')
  }

  /**
   * allocate resources for being able to write to the storage for a new dataset.
   * This method must be retryable: e.g. resources should only be created if they don't already exist
   */
  async initStorage(datasetId: string, fieldSchema: DatasetBinaryFieldSchema): Promise<void> {}

  /**
   * Delete all resources associated with a dataset
   * This method must be retryable: if a previous delete attempt was only partially applied an additial call
   *  in the future should pick up where the last one left of.
   *  initDataset will not be called with a given datasetId until removeDataset for that dataset was successful
   */
  async removeDataset(datasetId: string): Promise<void> {
    throw Error('not implemented')
  }

  /**
   * returns null if the file was not found
   */
  async read(fieldSchema: DatasetBinaryFieldSchema, datasetId: string, itemId: string): Promise<Readable | null> {
    throw Error('not implemented')
  }

  /**
   * when implemented returns a url that points to where the file is hosted.
   * this.hasUrl indicated whether this method is implemented or not
   */
  async getUrl(fieldSchema: DatasetBinaryFieldSchema, datasetId: string, itemId: string): Promise<string | null> {
    if (this.hasUrl) {
      throw Error('not implemented')
    } else {
      return null;
    }
  }
}

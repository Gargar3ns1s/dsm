
import { Readable, Stream } from "stream";
import { StorageBackendLocalDisk } from "../config";
import { DatasetBinaryFieldSchema } from "../_models";
import { StorageBackend } from "./storage_backend";
import { access, mkdir, rm, writeFile } from "fs/promises";
import { createReadStream, constants } from "fs";
import * as path from "path";


export class LocalDiskStorage extends StorageBackend {
  static readonly type = 'localdisk';

  protected readonly config: StorageBackendLocalDisk
  get hasUrl(): boolean {
    return !!this.config.hostedPath;
  }

  async write(fieldSchema: DatasetBinaryFieldSchema, datasetId: string, itemId: string, data: Stream): Promise<void> {
    const dataDir = path.join(this.config.localPath, datasetId, itemId);
    await mkdir(dataDir, { recursive: true });
    await writeFile(path.join(dataDir, fieldSchema.name), data);
  }

  async removeDataset(datasetId: string): Promise<void> {
    const dataPath = `${this.config.localPath}/${datasetId}/`;
    try {
      await access(dataPath, constants.F_OK);
    } catch(err) {
      console.warn(`dataset path does not exist: ${dataPath}`);
      return;
    }
    await rm(dataPath, {recursive: true});
  }

  async read(fieldSchema: DatasetBinaryFieldSchema, datasetId: string, itemId: string): Promise<Readable | null> {
    return createReadStream(path.join(this.config.localPath, datasetId, itemId, fieldSchema.name));
  }

  async getUrl(fieldSchema: DatasetBinaryFieldSchema, datasetId: string, itemId: string): Promise<string | null> {
    if (this.hasUrl) {
      return `${this.config.hostedPath}/${datasetId}/${itemId}/${fieldSchema.name}`
    } else {
      return null;
    }
  }
}


import {StorageBackend} from './storage_backend';
export {StorageBackend};

export const _STORAGE_BACKENDS: {[key: string]: typeof StorageBackend} = {};

import {LocalDiskStorage} from './local_disk_storage';
_STORAGE_BACKENDS[LocalDiskStorage.type] = LocalDiskStorage;
export {LocalDiskStorage}


export function getStorageBackend(type: string): typeof StorageBackend {
  return _STORAGE_BACKENDS[type];
}

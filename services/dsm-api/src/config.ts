
export interface DSMDatabaseConfig {
  client: 'mysql2'
  connection: {
    host: string
    port: number
    user: string
    password: string
    database: string
  }
}


export interface StorageBackendLocalDisk {
  type: 'localdisk'
  localPath: string
  hostedPath?: string   // optional: when set requests for reading data will get redirected to this endpoint
}

export type StorageBackendConfig = StorageBackendLocalDisk;

export interface DSMConfig {
  db: DSMDatabaseConfig;
  storageBackends: { [key: string]: StorageBackendConfig };
  // time window [ms] before an insert times out and is cleaned up
  commitDeadline: number;
};


export function getConfig(): DSMConfig {
  // TODO: read config from file if env var DMS_CONFIG is set
  return {
    db: {
      client: 'mysql2',
      connection: {
        host : 'dsm-mysql',
        port : 3306,
        user : 'dsm',
        password : 'pass',  // TODO: use actual password
        database : 'dsm-datasets'
      }
    },

    storageBackends: {
      // hosted: made accessable by some external service (e.g. nginx that reads from  ./data)
      default: {
        type: 'localdisk',
        localPath: '/data/',
      },
      // local data with no additional hosting
      localdisk: {
        type: 'localdisk',
        localPath: '/data/',
        hostedPath: '/binarydata',
      },
    },

    commitDeadline: 12 * 60 * 60 * 1000,
  }
}


import { DbModel } from './db_model';


export class ViewFieldSchema extends DbModel {
  name: string
  field: string
  via?: string

  constructor(data: Object) {
    super()
    // validate input and assign properties
    const name = data['name']
    if(typeof name != 'string')
      throw TypeError(`view field name must be a string, got ${typeof name}`)
    this.name = name

    const field = data['field'] ? data['field'] : data['name']
    if(typeof field != 'string')
      throw TypeError(`view field.field must be a string, got ${typeof field}`)
    this.field = field

    const via = data['via']
    if(via) {
      if(typeof via != 'string')
        throw TypeError(`view field via must be a string, got ${typeof via}`)
      this.via = via
    }
  }

  toObject(): Object {
    return {
      name: this.name,
      field: this.field,
      via: this.via
    }
  }
}

export class ViewModel extends DbModel {
  name: string
  base: string
  fields: ViewFieldSchema[]

  constructor(data: Object) {
    super()
    // validate input and assign properties
    const name = data['name']
    if(typeof name != 'string')
      throw TypeError(`view name must be a string, got ${typeof name}`)
    if(name.startsWith('_dsm'))
      throw TypeError(`"_dsm" prefixed names are reserved for management data structures`)
    this.name = name

    const base = data['base']
    if(typeof base != 'string')
      throw TypeError(`view base must be a string, got ${typeof base}`)
    this.base = base

    const fields = data['fields']
    if(!(fields instanceof Array))
      throw TypeError(`view expects an array of fields, got ${typeof fields}`)
    this.fields = fields.map((field) => new ViewFieldSchema(field))
  }

  toObject(): Object {
    return {
      name: this.name,
      base: this.base,
      fields: this.fields.map((f) => f.toObject()),
    }
  }

  toDb(): Object {
    return {
      name: this.name,
      base: this.base,
      fields: JSON.stringify(this.fields.map((f) => f.toDb())),
    }
  }

  static fromDb(data: Object): ViewModel {
    return new ViewModel({
      name: data['name'],
      base: data['base'],
      fields: JSON.parse(data['fields']),
    })
  }
}

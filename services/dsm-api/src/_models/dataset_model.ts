
import { identifierRegex } from 'src/common.dto';
import { DatasetFieldSchema, _DATASET_FIELD_SCHEMAS } from './dataset_fields';
import { DbModel } from './db_model';


export type DatasetSchema = DatasetFieldSchema[]

export class DatasetModel extends DbModel {
  name: string;
  schema: DatasetSchema;
  createdAt?: Date;
  created: boolean;
  deletedAt?: Date;

  constructor(data: Object) {
    super()
    // validate input and assign properties
    const name = data['name']
    if(typeof name != 'string')
      throw TypeError(`dataset name must be a string, got ${typeof name}`)
    if (!identifierRegex.test(name))
      throw TypeError(`invalid name`)
    this.name = name

    const schema = data['schema']
    if(!(schema instanceof Array))
      throw TypeError(`dataset schema expects an array of field schema, got ${typeof schema}`)
    this.schema = schema.map((field, idx) => {
      const type = field['type']
      if(typeof type != 'string')
        throw TypeError(`missing type for field at index ${idx}`)
      for (const SchemaCls of _DATASET_FIELD_SCHEMAS) {
        if (type === SchemaCls.type)
          return new SchemaCls(field)
      }
      throw TypeError(`unknwon field type ${type}`)
    })
    // check there are any duplicate field names
    const fieldNames = this.schema.map((v) => v.name)
    if (fieldNames.length !== (new Set(fieldNames)).size) {
      throw TypeError(`duplicate field name in schema`)
    }
  }

  toObject(): {} {
    return {
      name: this.name,
      schema: this.schema.map((f) => f.toObject()),
      createdAt: this.createdAt ? this.createdAt.toUTCString() : undefined,
      created: this.created,
      deletedAt: this.deletedAt ? this.deletedAt.toUTCString() : undefined
    }
  }

  toDb(): Object {
    return {
      name: this.name,
      schema: JSON.stringify(this.schema.map((f) => f.toDb())),
    }
  }

  static fromDb(data: Object): DatasetModel {
    const dataset = new DatasetModel({
      name: data['name'],
      schema: JSON.parse(data['schema']),
    })
    dataset.createdAt = new Date(data['createdAt']);
    dataset.created = !!data['created'],
    dataset.deletedAt = data['deletedAt'] ? new Date(data['deletedAt']) : undefined;
    return dataset
  }
}

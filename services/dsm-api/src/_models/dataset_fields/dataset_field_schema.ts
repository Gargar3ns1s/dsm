
import { identifierRegex } from 'src/common.dto';
import { DbModel } from '../db_model';


export class DatasetFieldSchema extends DbModel {
  static readonly type: string; // set in subclasses

  name: string

  get type(): string {
    const type = (<typeof DatasetFieldSchema>this.constructor).type
    if (!type)
      throw Error('field type not implemented')
    return type
  }

  constructor(data: Object) {
    super()
    const name = data['name']
    if(typeof name != 'string')
      throw TypeError(`repo field name must be a string, got ${typeof name}`)
    if (!identifierRegex.test(name))
      throw TypeError(`invalid name`)
    this.name = name
  }

  toObject(): Object {
    return {
      name: this.name,
      type: this.type
    }
  }

  static fromDb(data: Object): DatasetFieldSchema {
    return new DatasetFieldSchema(data)
  }
}


import { DatasetFieldSchema } from './dataset_field_schema'


export class DatasetStringFieldSchema extends DatasetFieldSchema {
  static readonly type = 'string'

  length: number     // maximum length of the string
  regex?: RegExp

  constructor(data: Object) {
    super(data)

    const length = data['length']
    if(length) {
      if (typeof length != 'number')
        throw TypeError(`repo string field length property must be a number, got ${typeof length}`)
      this.length = length
    }
    const regex = data['regex']
    if(regex) {
      const tmp = regex.split('/');
      const modifiers = tmp.pop()
      tmp.shift()
      this.regex = new RegExp(tmp.join('/'), modifiers)
    }
  }

  toObject(): Object {
    const res = super.toObject()
    if(this.length !== undefined)
      res['length'] = this.length
    if(this.regex)
      res['regex'] = `${this.regex}`;
    return res
  }

  static fromDb(data: Object): DatasetStringFieldSchema {
    return new DatasetStringFieldSchema(data)
  }
}

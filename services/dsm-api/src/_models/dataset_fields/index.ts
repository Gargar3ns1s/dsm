
import {DatasetFieldSchema} from './dataset_field_schema'
export {DatasetFieldSchema}

export const _DATASET_FIELD_SCHEMAS: (typeof DatasetFieldSchema)[] = []

import {DatasetStringFieldSchema} from './dataset_string_field_schema'
_DATASET_FIELD_SCHEMAS.push(DatasetStringFieldSchema)
export {DatasetStringFieldSchema}

import {DatasetReferenceFieldSchema} from './dataset_reference_field_schema'
_DATASET_FIELD_SCHEMAS.push(DatasetReferenceFieldSchema)
export {DatasetReferenceFieldSchema}

import {DatasetBinaryFieldSchema} from './dataset_binary_field_schema'
_DATASET_FIELD_SCHEMAS.push(DatasetBinaryFieldSchema)
export {DatasetBinaryFieldSchema}

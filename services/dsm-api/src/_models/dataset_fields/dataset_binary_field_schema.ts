
import { DatasetFieldSchema } from './dataset_field_schema'


export class DatasetBinaryFieldSchema extends DatasetFieldSchema {
  static readonly type = 'binary'

  mime: string
  storage: string   // storage backend that should be used for this dataset
  length?: number   // maximum size in bytes

  constructor(data: Object) {
    super(data)

    const mime = data['mime']
    if(typeof mime != 'string')
      throw TypeError(`repo field mime must be a string, got ${typeof mime}`)
    this.mime = mime

    const storage = data['storage']
    if(typeof storage != 'string')
      throw TypeError(`repo field storage must be a string, got ${typeof storage}`)
    this.storage = storage

    const length = data['length']
    if(length) {
      if (typeof length != 'number')
        throw TypeError(`repo string field length property must be a number, got ${typeof length}`)
      this.length = length
    }
  }

  toObject(): Object {
    const res = super.toObject()
    res['mime'] = `${this.mime}`;
    res['storage'] = `${this.storage}`;
    if(this.length !== undefined)
      res['length'] = this.length
    return res
  }

  static fromDb(data: Object): DatasetBinaryFieldSchema {
    return new DatasetBinaryFieldSchema(data)
  }
}

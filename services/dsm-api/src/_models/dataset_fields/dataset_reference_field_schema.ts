
import { DatasetFieldSchema } from './dataset_field_schema'


export class DatasetReferenceFieldSchema extends DatasetFieldSchema {
  static readonly type = 'reference'

  target: string  // the target dataset name

  constructor(data: Object) {
    super(data)

    const target = data['target']
    if(target) {
      if(typeof target != 'string')
        throw TypeError(`reference dataset must be a string, got ${typeof target}`)
      this.target = target
    } else {
      this.target = this.name
    }
  }

  toObject(): Object {
    const res = super.toObject()
    res['target'] = this.target
    return res
  }

  static fromDb(data: Object): DatasetReferenceFieldSchema {
    return new DatasetReferenceFieldSchema(data)
  }
}

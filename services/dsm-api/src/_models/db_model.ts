
export abstract class DbModel {

  // serilaize instance for sending it to the client
  abstract toObject(): Object

  // seriallize instance for sending it to the database (e.g. for use with knex.insert())
  toDb(): Object {
    return this.toObject()
  }

  copy(): this {
    return Object.assign(Object.create(Object.getPrototypeOf(this)), this)
  }
}

import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString, Length } from "class-validator";
import { NamedResourceDto } from "src/common.dto";
import { DatasetShortInfoDto } from "src/dataset/dataset.dto";


export class ViewShortInfoDto extends DatasetShortInfoDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  base: string;
}


export class ViewFieldDto extends NamedResourceDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  field: string

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  @Length(1, 255)
  via?: string
}

export class AddViewDto extends NamedResourceDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  base: string;

  @ApiProperty({ type: [ViewFieldDto] })
  fields: ViewFieldDto[]
}

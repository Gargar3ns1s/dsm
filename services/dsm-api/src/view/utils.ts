import { HttpException, HttpStatus } from "@nestjs/common"
import { DatasetReferenceFieldSchema, DatasetSchema } from "src/_models"


export function refernceFieldSchemaByName(schema: DatasetSchema, name: string): DatasetReferenceFieldSchema {
  const res = <DatasetReferenceFieldSchema>schema.find((field) => field.type == 'reference' && field.name == name)
  if (res) {
    return res
  } else {
    throw new HttpException(
      `reference field "${name}" not found in schema "${schema ? schema.map((s) => s.name) : undefined}"`,
      HttpStatus.UNPROCESSABLE_ENTITY)
  }
}

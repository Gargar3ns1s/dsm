
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Query, Res } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { NamedResourceDto, PageQueryDto } from 'src/common.dto';
import { wrapHttpErrorAsyncFn } from 'src/controller_utils';
import { DatabaseService } from 'src/database.service';
import { AddViewDto, ViewShortInfoDto } from './view.dto';
import { ViewModel } from 'src/_models/view_model';
import { ViewService } from './view.service';
import { Response } from 'express';


@Controller('/view')
@ApiTags('view')
export class ViewController {
  constructor(protected readonly db: DatabaseService,
              protected readonly views: ViewService) {}

  @Get('/')
  @ApiOperation({
    summary: 'list views',
    description: [
      'List all available and ready views with minimal info. Similar to *list repositories*',,
    ].join('  \n'),
  })
  @ApiResponse({
    status: 200,
    description: 'get minimal info for all available views',
    type: [ ViewShortInfoDto ],
  })
  async listIds(@Query() pagination: PageQueryDto): Promise<ViewShortInfoDto[]> {
    return wrapHttpErrorAsyncFn(async() => this.views.listMinimalInfo(pagination));
  }

  @Post('/')
  @ApiOperation({
    summary: 'create view',
    description: [
      'Create a new view',
      'error/retry semantics are identical to *create repository*'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'add a new repository',
    type: ViewShortInfoDto,
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'repository with a matching schema definition already exists, returns the existing repo',
    type: ViewShortInfoDto,
  })
  @ApiResponse({
    status: HttpStatus.CONFLICT,
    description: 'repository with the same name exists but the schema differs to the one defined in this request, return the conflicting repo',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async add(@Body() data: AddViewDto, @Res() res: Response): Promise<void> {
    return await wrapHttpErrorAsyncFn(async() => {
      const newView = new ViewModel(data);
      const {created, dataset, view} = await this.views.addView(newView);
      res.status(created ? HttpStatus.CREATED : HttpStatus.OK);
      res.send({ name: dataset.name, createdAt: dataset.createdAt, base: view.base })
    });
  }

  @Delete('/:datasetId')
  @ApiOperation({
    summary: 'delete view',
    description: [
      'Delete a view',
      'error/retry semantics are identical to *delete repository*'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'deletion successful',
    type: NamedResourceDto,
  })
  @ApiResponse({
    status: 404,
    description: 'view not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async delete(@Param('datasetId') datasetId: string): Promise<NamedResourceDto> {
    return await wrapHttpErrorAsyncFn(async() => {
      await this.views.deleteView(datasetId);
      return { name: datasetId }
    })
  }
}

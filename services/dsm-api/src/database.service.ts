
import { Injectable } from '@nestjs/common';
import { knex, Knex } from 'knex';
import { getConfig } from './config';


@Injectable()
export class DatabaseService {
  private _config = getConfig().db;

  client: Knex = knex({
    ...this._config,
  })
}


import { Controller, HttpStatus, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CleanupService } from './cleanup.service';


@Controller('system')
@ApiTags('system')
export class SystemController {
  constructor(protected readonly cleanupService: CleanupService) {}

  @Post('/cleanup')
  @ApiOperation({
    summary: 'run cleanup jobs',
    description: [
      'This call allows manually triggering cleanup.',
      'Usually it is called periodically by a cron job to keep everything tidy.'
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'repository with a matching schema definition already exists, returns the existing repo',
    type: Object,
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async cleanup(): Promise<{}> {
    await this.cleanupService.runAll();
    return {};
  }
}

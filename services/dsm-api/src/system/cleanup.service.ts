
import { DatabaseService } from 'src/database.service';
import { Injectable } from '@nestjs/common';
import { getConfig } from 'src/config';


@Injectable()
export class CleanupService {
  private _config = getConfig();

  constructor(protected readonly db: DatabaseService) {}

  async runAll(): Promise<void> {
    // TODO: implement
  }

  async removeStaleUncomittedItems(): Promise<void> {
    // TODO: implement
  }

  async removeStaleCreatingDatasets(): Promise<void> {
    // TODO: implement
  }

  async runDatasetDeletes(): Promise<void> {
    // TODO: implement
  }
}

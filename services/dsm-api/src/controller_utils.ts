
import { HttpException, HttpStatus } from "@nestjs/common";
import { PageQueryDto } from "./common.dto";

export function wrapHttpError(err: any) {
  let message = ''
  if (err) {
    if (err instanceof HttpException) {
      return err
    } else {
      if (err['message']) {
        message = `${err['message']}`;
      } else {
        message = `${err}`;
      }
    }
  }
  return new HttpException(message, HttpStatus.INTERNAL_SERVER_ERROR);
}

export function wrapHttpErrorFn<T>(fn: () => T): T {
  try {
    return fn();
  } catch(err) {
    console.trace(err);
    throw wrapHttpError(err);
  }
}

export async function wrapHttpErrorAsyncFn<T>(fn: () => Promise<T>): Promise<T> {
  try {
    return await fn();
  } catch(err) {
    console.trace(err);
    throw wrapHttpError(err);
  }
}

export interface IPageQuery {
  offset: number;
  limit: number;
}
export function getPageQuery(pagination: PageQueryDto, maxPageSize: number = 1000): IPageQuery {
  const pageIdx = pagination.page;
  const limit = Math.min(pagination.pageSize, maxPageSize);
  return {
    offset: pageIdx * limit,
    limit,
  }
}

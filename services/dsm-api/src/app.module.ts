import { Module } from '@nestjs/common';
import { DatabaseService } from './database.service';
import { RepoController } from './repo/repo.controller';
import { DatasetController } from './dataset/dataset.controller';
import { ViewController } from './view/view.controller';
import { RepoService } from './repo/repo.service';
import { DatasetService } from './dataset/dataset.service';
import { ViewService } from './view/view.service';
import { SystemController } from './system/system.controller';
import { CleanupService } from './system/cleanup.service';
import { DatasetItemService } from './dataset/dataset_item.service';
import { ItemInsertService } from './repo/item_insert.service';

@Module({
  imports: [],
  controllers: [
    RepoController,
    DatasetController,
    ViewController,
    SystemController,
  ],
  providers: [
    DatabaseService,
    RepoService,
    ItemInsertService,
    DatasetService,
    DatasetItemService,
    ViewService,
    CleanupService,
  ],
})
export class AppModule {}

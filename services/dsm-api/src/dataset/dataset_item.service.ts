
import { Injectable, NotFoundException, StreamableFile } from '@nestjs/common';
import { DatasetBinaryFieldSchema, DatasetModel, DatasetReferenceFieldSchema, DatasetSchema, DatasetStringFieldSchema } from 'src/_models';
import { PageQueryDto } from 'src/common.dto';
import { DatabaseService } from 'src/database.service';
import { DatasetItemDto, ItemCountDto } from './dataset.dto';
import { getPageQuery } from 'src/controller_utils';
import { DatasetService } from './dataset.service';
import { StorageBackend, getStorageBackend } from 'src/_storage_backends';
import { getConfig } from 'src/config';


@Injectable()
export class DatasetItemService {
  protected readonly _config = getConfig();
  protected readonly _storageBackends: { [key: string]: StorageBackend };

  constructor(protected readonly db: DatabaseService,
              protected readonly datasets: DatasetService) {
      this._storageBackends = {};
      for (const [storageName, storageConfig] of Object.entries(this._config.storageBackends)) {
        const backend = new (getStorageBackend(storageConfig.type))(storageConfig)
        this._storageBackends[storageName] = backend
      }
    }

  protected _buildItem(schema: DatasetSchema, data: Object): DatasetItemDto {
    const res = {
      id: `${data['_dsm_id']}`,
      committedAt: new Date(data['_dsm_committedAt']),
      data: {}
    };
    for (const field of schema) {
      switch (field.type) {
        case 'string':
        case 'reference':
          res.data[field.name] = data[field.name];
          break;
        case 'binary':
          res.data[field.name] = (<Buffer>data[field.name]).toString('hex');
          break;
        default:
          throw new Error('field type not implemented')
      }
    }
    return res;
  }

  protected async _ensureDataset(datasetId: string): Promise<DatasetModel> {
    const dataset = await this.datasets.getDataset(datasetId);
    if (!dataset)
      throw new NotFoundException(`dataset not found`)
    return dataset;
  }

  async list(datasetId: string, pagination: PageQueryDto): Promise<DatasetItemDto[]> {
    const pageQuery = getPageQuery(pagination, 1000);
    const dataset = await this._ensureDataset(datasetId);
    const dbResult =  await this.db.client
      .table(datasetId)
      .select('*')
      .where('_dsm_committed', true)
      .offset(pageQuery.offset).limit(pageQuery.limit);
    return dbResult.map((data) => this._buildItem(dataset.schema, data))
  }

  async getItem(datasetId: string, itemId: string): Promise<DatasetItemDto> {
    const dataset = await this._ensureDataset(datasetId);
    const dbResult = await this.db.client
      .table(datasetId)
      .select('*')
      .where('_dsm_id', itemId)
      .where('_dsm_committed', true)
    if (!dbResult || !dbResult.length)
      throw new NotFoundException(`item not found`)

    return this._buildItem(dataset.schema, dbResult[0])
  }

  async getItemField( datasetId: string,
                      itemId: string,
                      fieldName: string
                    ): Promise<{ data: string | StreamableFile, mimeType: string, redirect?: string}> {
    const dataset = await this._ensureDataset(datasetId);
    // get field schema
    const fieldSchema = dataset.schema.find((v) => v.name === fieldName);
    if (!fieldSchema)
      throw new NotFoundException(`field not in dataset schema`);

    // read data
    const dbResult =  await this.db.client
      .table(datasetId)
      .select(fieldName)
      .where('_dsm_id', itemId)
      .where('_dsm_committed', true)
      .limit(1)
    if (!dbResult || !dbResult.length)
      throw new NotFoundException(`item not found`)

    // build response depending on ite type
    if (fieldSchema instanceof DatasetStringFieldSchema ||
        fieldSchema instanceof DatasetReferenceFieldSchema) {
      return {
        data: dbResult[0][fieldSchema.name],
        mimeType: 'text/plain',
      }
    } else if (fieldSchema instanceof DatasetBinaryFieldSchema) {

      const storage = this._storageBackends[(<DatasetBinaryFieldSchema>fieldSchema).storage];
        if (!storage)
          throw new Error(`storage not found`);

        if (storage.hasUrl) {
          // redirect
          return {
            data: '',
            mimeType: fieldSchema.mime,
            redirect: await storage.getUrl(<DatasetBinaryFieldSchema>fieldSchema, datasetId, itemId)
          }
        } else {
          const stream = await storage.read(<DatasetBinaryFieldSchema>fieldSchema, datasetId, itemId);
          return {
            data: new StreamableFile(stream),
            mimeType: fieldSchema.mime
          }
        }
    } else {
      throw new Error('field type not implemented')
    }
  }

  async countItems(datasetId: string): Promise<ItemCountDto> {
    const dbResult = await this.db.client
      .table(datasetId)
      .where('_dsm_committed', true)
      .count({ totalItems: '*' })
      .catch(null);
    if (!dbResult || !dbResult.length)
      throw new NotFoundException(`item not found`);
    return dbResult[0]
  }
}

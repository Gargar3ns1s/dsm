
import { Controller, Get, HttpStatus, Param, Query, Res, StreamableFile } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { StorageBackend, getStorageBackend } from 'src/_storage_backends';
import { getConfig } from 'src/config';
import { wrapHttpErrorAsyncFn } from 'src/controller_utils';
import { DatabaseService } from 'src/database.service';
import { DatasetInfoDto, DatasetItemDto, DatasetShortInfoDto, ItemCountDto } from './dataset.dto';
import { PageQueryDto } from 'src/common.dto';
import { DatasetService } from './dataset.service';
import { DatasetItemService } from './dataset_item.service';
import { Response } from 'express';


@Controller('/dataset')
@ApiTags('dataset')
export class DatasetController {
  protected readonly _config = getConfig();
  protected readonly _storageBackends: { [key: string]: StorageBackend };

  constructor(protected readonly db: DatabaseService,
              protected readonly datasets: DatasetService,
              protected readonly items: DatasetItemService) {
    this._storageBackends = {};
    for (const [storageName, storageConfig] of Object.entries(this._config.storageBackends)) {
      const backend = new (getStorageBackend(storageConfig.type))(storageConfig)
      this._storageBackends[storageName] = backend
    }
  }

  @Get('/')
  @ApiOperation({
    summary: 'list datasets',
    description: [
      'List all available datasets (views and repos) with minimal info.',
      'Datasets that are __not__ in a clean state (not completely created; partially deleted) are excluded',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get minimal info for all available repositories',
    type: [ DatasetShortInfoDto ],
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async listIds(@Query() pagination: PageQueryDto): Promise<DatasetShortInfoDto[]> {
    return wrapHttpErrorAsyncFn(async() => this.datasets.listMinimalInfo(pagination));
  }

  @Get('/:datasetId')
  @ApiOperation({
    summary: 'get datasets info',
    description: [
      'Get detailed info about a dataset and it\'s dependencies',
      'Note that references to or referencing datasets are __included__ in the *referencesTo* or *referencedBy* \
        arrays even if they are __not fully created__. This is because for datasets in creation we expect the new \
        reference to become factual in the future and thus a dataset even though it is not fully created already \
        blocks deletion of it\'s dependencies',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: DatasetInfoDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'dataset not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async getInfo(@Param('datasetId') datasetId: string): Promise<DatasetInfoDto> {
    return wrapHttpErrorAsyncFn(async() => this.datasets.getDatasetInfo(datasetId));
  }

  @Get('/:datasetId/item')
  @ApiOperation({
    summary: 'list items in the dataset',
    description: [
      'Get a list of items in a dataset',
      '- string fields are returned as-is',
      '- binary fields are resolved to the sha256 checksum of their content',
      '- reference fields are resolved to the item id from the dataset they are referencing',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: [ DatasetItemDto ]
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'dataset or item not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async listItems(@Param('datasetId') datasetId: string,
                  @Query() pagination: PageQueryDto): Promise<DatasetItemDto[]> {
    return wrapHttpErrorAsyncFn(async() => this.items.list(datasetId, pagination));
  }

  @Get('/:datasetId/item/:itemId')
  @ApiOperation({
    summary: 'get an item from the dataset',
    description: [
      'Receive the item from the dataset',
      'Binary data fields contain the sha256 sum of their associated data. The data itself can be accessed through\
        the field get endpoint',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: DatasetItemDto
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'dataset not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async item(@Param('datasetId') datasetId: string,
             @Param('itemId') itemId: string): Promise<DatasetItemDto> {
    return wrapHttpErrorAsyncFn(async() => this.items.getItem(datasetId, itemId));
  }

  @Get('/:datasetId/item/:itemId/:fieldName')
  @ApiOperation({
    summary: 'get data for a single field from an item',
    description: [
      'Download data form and item',
      'For binary data the endpoint may return a redirect to where the data is hosted',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'dataset not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async itemField(@Param('datasetId') datasetId: string,
                  @Param('itemId') itemId: string,
                  @Param('fieldName') fieldName: string,
                  @Res({ passthrough: true }) res: Response,
                 ): Promise<string | StreamableFile> {
    return wrapHttpErrorAsyncFn(async() => {
      const {data, mimeType, redirect} = await this.items.getItemField(datasetId, itemId, fieldName);
      if (redirect) {
        res.redirect(redirect);
      } else {
        res.set({ 'Content-Type': mimeType });
        return data;
      }
    });
  }

  @Get('/:datasetId/count')
  @ApiOperation({
    summary: 'count items in the dataset',
    description: [
      'Get number of all committed items in the dataset',
    ].join('  \n'),
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: ItemCountDto
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'dataset not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'unknown error',
  })
  async itemCount(@Param('datasetId') datasetId: string): Promise<ItemCountDto> {
    return wrapHttpErrorAsyncFn(async() => this.items.countItems(datasetId));
  }
}

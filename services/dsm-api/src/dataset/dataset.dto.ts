
import { ApiExtraModels, ApiProperty, getSchemaPath } from "@nestjs/swagger";
import { IsString, Length, IsDate, IsInt, IsMimeType, IsOptional, Max } from 'class-validator';
import { DatasetBinaryFieldSchema, DatasetReferenceFieldSchema, DatasetStringFieldSchema } from "src/_models";
import { NamedResourceDto } from "src/common.dto";


export class DatasetFieldSchemaBaseDto extends NamedResourceDto {
  @IsString()
  @Length(1, 64)
  type: string;
}

export class DatasetStringFieldSchemaDto extends DatasetFieldSchemaBaseDto {
  @ApiProperty({ example: DatasetStringFieldSchema.type })
  type: 'string' = DatasetStringFieldSchema.type;

  @ApiProperty({})
  @IsInt()
  @Max(4096)
  length: number;

  @ApiProperty({ required: false, example: '/^.*/' })
  @IsOptional()
  @IsString()
  regex?: string;
}

export class DatasetBinaryFieldSchemaDto extends DatasetFieldSchemaBaseDto {
  @ApiProperty({ example: DatasetBinaryFieldSchema.type })
  type: 'binary' = DatasetBinaryFieldSchema.type;

  @ApiProperty({ example: 'image/jpeg' })
  @IsMimeType()
  mime: string;

  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  storage: string

  @ApiProperty({ required: false })
  @IsOptional()
  @IsInt()
  length?: number
}

export class DatasetReferenceFieldSchemaDto extends DatasetFieldSchemaBaseDto {
  @ApiProperty({ example: DatasetReferenceFieldSchema.type })
  type: 'reference' = DatasetReferenceFieldSchema.type;

  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  target: string;
}


export type DatasetFieldSchemaDto = DatasetStringFieldSchemaDto | DatasetBinaryFieldSchemaDto | DatasetReferenceFieldSchemaDto;


export const datasetSchemaApiPropertyConfig = {
  type: 'array',
  items: {
    oneOf: [
      { $ref: getSchemaPath(DatasetStringFieldSchemaDto) },
      { $ref: getSchemaPath(DatasetBinaryFieldSchemaDto) },
      { $ref: getSchemaPath(DatasetReferenceFieldSchemaDto) },
    ],
    discriminator: {
      propertyName: 'type',
      mapping: {
        [DatasetStringFieldSchema.type]: DatasetStringFieldSchemaDto.constructor.name,
        [DatasetBinaryFieldSchema.type]: DatasetBinaryFieldSchemaDto.constructor.name,
        [DatasetReferenceFieldSchema.type]: DatasetReferenceFieldSchemaDto.constructor.name,
      }
    }
  },
};

export class RepoInfoDto extends NamedResourceDto {
}

export class DatasetShortInfoDto extends RepoInfoDto {
  @ApiProperty({})
  @IsDate()
  createdAt: Date;
}

export class ViewInfoDto extends RepoInfoDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  field: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  @Length(1, 255)
  via?: string
}


@ApiExtraModels(DatasetStringFieldSchemaDto)
@ApiExtraModels(DatasetBinaryFieldSchemaDto)
@ApiExtraModels(DatasetReferenceFieldSchemaDto)
export class DatasetInfoDto extends DatasetShortInfoDto {
  @ApiProperty(datasetSchemaApiPropertyConfig)
  schema: DatasetFieldSchemaDto[];

  @ApiProperty({ required: false, type: RepoInfoDto })
  @IsOptional()
  repo?: RepoInfoDto;

  @ApiProperty({ required: false, type: ViewInfoDto })
  @IsOptional()
  view?: ViewInfoDto;

  @ApiProperty({})
  referencesTo: string[];

  @ApiProperty({})
  referencedBy: string[];
}


export class DatasetItemDto {
  @ApiProperty({})
  @IsString()
  @Length(1, 255)
  id: string;

  @ApiProperty({})
  @IsDate()
  committedAt: Date;

  @ApiProperty({
    type: 'object',
    additionalProperties: { type: 'string' }})
  data: { [key: string]: string };
}


export class ItemCountDto {
  @ApiProperty({})
  @IsInt()
  totalItems: number;
}

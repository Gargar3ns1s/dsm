
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { DatasetFieldSchema, DatasetModel } from 'src/_models';
import { PageQueryDto, identifierRegex } from 'src/common.dto';
import { DatabaseService } from 'src/database.service';
import { DatasetInfoDto, DatasetShortInfoDto, RepoInfoDto, ViewInfoDto } from './dataset.dto';
import { getPageQuery } from 'src/controller_utils';
import { ViewModel } from 'src/_models/view_model';


@Injectable()
export class DatasetService {
  constructor(protected readonly db: DatabaseService) {}

  /**
   * checks a dataset id, error if invlaid
   */
  checkDatasetId(datasetId: string): void {
    if (!identifierRegex.test(datasetId))
      throw new HttpException('invalid datasetId', HttpStatus.BAD_REQUEST)
  }

  /**
   * check if schemas are equal (deep equal; ignores order of field definitions)
   */
  schemaEqual(schemaA: DatasetFieldSchema[], schemaB: DatasetFieldSchema[]): boolean {
    function sortByName(a: DatasetFieldSchema, b: DatasetFieldSchema): number {
      if (a.name < b.name)
        return -1;
      if (a.name > b.name)
        return 1;
      return 0;
    }
    return (JSON.stringify(schemaA.sort(sortByName)) === JSON.stringify(schemaB.sort(sortByName)));
  }

  async listMinimalInfo(pagination: PageQueryDto): Promise<DatasetShortInfoDto[]> {
    const pageQuery = getPageQuery(pagination, 1000);
    return await this.db.client
      .table('_dsm_datasets')
      .select('name', 'createdAt')
      // filter out deleted & creating datasets
      .where({
        created: true,
        deletedAt: null,
      })
      .offset(pageQuery.offset).limit(pageQuery.limit);
  }

  async getDataset( datasetId: string,
                    options: {
                      includeCreating?: boolean,
                      includeDeleting?: boolean,
                      transaction?: Knex.Transaction,
                    } = {}
                  ): Promise<DatasetModel | null> {
    this.checkDatasetId(datasetId)
    const dbClient = options.transaction || this.db.client;

    // TODO: cache DatasetModel instances
    const whereFilter = { name: datasetId };
    if (!options.includeCreating)
      whereFilter['created'] = true;
    if (!options.includeDeleting)
      whereFilter['deletedAt'] = null;
    const dbResult = await dbClient
      .table('_dsm_datasets')
      .select('*')
      .where(whereFilter)
      .limit(1);

    if (!dbResult || !dbResult.length) {
      return null;
    }
    return DatasetModel.fromDb(dbResult[0]);
  }

  async getDatasetInfo(datasetId: string): Promise<DatasetInfoDto> {
    const dataset = await this.getDataset(datasetId);
    if (!dataset)
      throw new HttpException(`dataset not found`, HttpStatus.NOT_FOUND)

    const [dbResultRepo, dbResultView, dbResultRefsTo, dbResultRefedBy] = await Promise.all([
      // load repo data
      this.db.client
        .table('_dsm_repos')
        .select('*')
        .where({ name: dataset.name })
        .limit(1),
      // load view data
      this.db.client
        .table('_dsm_views')
        .select('*')
        .where({ name: dataset.name })
        .limit(1),
      // load referenced datasets
      this.db.client
        .table('_dsm_refs')
        .select('target')
        .where({ origin: dataset.name }),
      // load dataset that reference this dataset
      this.db.client
        .table('_dsm_refs')
        .select('origin')
        .where({ target: dataset.name })
      ]);

    return <DatasetInfoDto>{
      ...dataset.toObject(),
      repo: dbResultRepo.length ? <RepoInfoDto>dbResultRepo[0] : undefined,
      view: dbResultView.length ? <ViewInfoDto>ViewModel.fromDb(dbResultView[0]).toObject() : undefined,
      referencesTo: <string[]>dbResultRefsTo.map((v) => v['target']),
      referencedBy: <string[]>dbResultRefedBy.map((v) => v['origin']),
    };
  }

  async ensureDatasetDeletable( datasetId: string,
                                options: {
                                  transaction?: Knex.Transaction,
                                } = {}
                              ): Promise<void> {
    this.checkDatasetId(datasetId)
    const dbClient = options.transaction || this.db.client;

    // check if dataset exists
    if (!(await this.getDataset(datasetId, {transaction: options.transaction, includeCreating: true, includeDeleting: true})))
      throw new HttpException('dataset does not exist', HttpStatus.NOT_FOUND);

    // check if dataset is referenced
    const refs = await dbClient
      .table('_dsm_refs')
      .select('origin')
      .where('target', datasetId);
    if (refs.length)
      throw new HttpException(
        `dataset is referenced by ${refs.map((x) => x['origin']).join(', ')}`,
        HttpStatus.CONFLICT);
  }
}

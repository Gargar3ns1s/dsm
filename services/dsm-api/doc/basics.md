
# Glossary
dataset: an indexed collection of data
repository: a dataset that stores actual data (items allocate storage space); items can contain references to other repositories
views: a dataset that references data in other datasets (query)

# general concept:
## 2 types of datasets:
1.) Repositories
    Contain actual data; every item corresponds to some portion of allocated (disk) storage space.
    Data in a dataset has a fixed schema that is specified once and can't be changed afterwards
    Deleting a dataset will delete all items in the dataset; allocated storage space will be freed.
    Deleting items in a dataset will free the associated storage space
2.) Views
    Are defined by a query that references one or more datasets (repositories and/or views).
    Views are intentionally limited to operations that do not change the result set (e.g. given the view is based on a dataset with X items, the view will contain X items as well and there is no was it can ever contain more or less <-> filter/merge operations are not possible). This limitation was choosen to allow the DSM to efficiently produce change notifications, which in turn allows operations like filter and merge to be implemented as an external transforms.

### field types
1.) __string__ fields
    are meant as a way to store searchable/orderable information on a datapoint. This can e.g. be a label, date, item count, file path, ...
    It is not suited for storing a large amount of data - use it for imposing a structure on the items within a dataset.
    Data on string fields is stored directly in the relational database. It can be indexed and is thus tailored towards fast access for sorting and filtering - the flipside is that the database will get slower if it has to cache a lot of data. So keep string data small
2.) __binary__ fields
    are suited for storing large amounts of data. Things like images, videos, caption texts, rich annotation files (e.g. coco boxes), ... go here.
    Binary data is stored on dedicated blob storage infrastructure that scales well for both amount of data stored and number of objects stored.
    Checksums for data stored in binary fields is stored in the database, allowing to validate data integrity
3.) __reference__ fields
    allow referring to data points in another dataset (fixed dataset per field). This field is key to documenting relationships between datasets. A dataset that is referenced by other datasets can not be deleted!

## tansforms
[TODO / not implemented]
DSM provides replayable change notifications for all datasets.
This allows for simple and fault tolerant implementation of transforms on datasets by external services (custom code / custom deployment). These transforms allow a lot of freedom.
Example use cases are:
    - process data (e.g. convert image to other format; extract video frame; train an ML model, ...)
    - request data to be labeled by a human
    - filter by some property
    - slice data into multiple datasets (e.g. create a new dataset for every month)
    - merge data from multiple datasets
    - ...

# resource names
- all names (dataset name, field name) may use characters: `[A-Z][a-z][0-9]-_~`
    This ensures that names can be used to form valid parts of a url and can be used in SQL queries
- datset and field names may not start with `_dsm_`


# resource state integrity

## dataset model state transitions
All dataset (view/repo) create and delete operations can be retried and will resume where they left of. This way any inconsistency caused by partially failed operations can be resolved.
These properties ensure byzantine fault tolerance for create and delete operations.

### create
- new datasets get created with their `created` property set to false. This marks them as "dangling", meaning that the creation has started but was not completed successfully.
- references get registered when creation starts - the rationale is that we assume reation will finish successfully at some point in the future. This means that other datasets that are referenced by this dataset get blocked from being deleted even if this dataset is dangling.
- all additional resources (e.g. storage bucket, message queue, database table/view, ...) for a dataset get created on a "create if not present" basis. This means that creation of a dataset can be retried (provided that schemas match) to resolve a dangling state.
- Once all resources have been created sucessfully, the dataset's `created` property is set to true, marking it as ready.
- Dangling datasets are can not be read from or written to (they are invisible for all item operations)

### delete
- for deletion, a dataset get's it's `deletedAt` property set to the current date. Marking it as deletion in progress.
- any recorded references of this dataset will be removed right away. The rationale behind this is that even though resources from the dataset are not cleaned up yet, there is no way for this dataset to become visible and usable for item operations again - the only possible future for it is to become "more deleted"
- all additional resources for the dataset get cleaned up on a "remove if present" basis. Similar to creation, this means that deletion can be retried/resumed.
- once all resources have been cleaned up, the dataset entry itself gets removed. This means that the name of the dataset is still taken while the dataset is getting deleted (meaning that no dataset with the same name can be created until deletion is complete)
- Datasets marked as deleted (similar to dangling state) can not be read from or written to

### automated cleanup
- [TODO:] all datasets that are in creation dangling state for more then some time (default: 1h) will be deleted automatically. This deleteion is handled by the normal delete procedure (calling the delete api endpoint)
- [TODO:] for all datasets that are in delete state for more then some time (default: 1h), deletion will be retried until the delete is complete and the resource is gone


## item add transaction
TODO

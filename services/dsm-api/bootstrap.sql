
/*
// TODO:
- make sure these commands don't override existing properies
- run automatically on rollout (job?, init container?)
*/

/* create user "dsm" */
CREATE USER IF NOT EXISTS 'dsm'@'%' IDENTIFIED WITH mysql_native_password;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE,
INDEX, REFERENCES, ALTER, CREATE VIEW, SHOW VIEW, EXECUTE ON *.* TO 'dsm'@'%';
SET PASSWORD FOR 'dsm'@'%' = 'pass'; /* TODO: set actual password */

/* create databases */
CREATE DATABASE IF NOT EXISTS `dsm-datasets` CHARACTER SET UTF8MB4 COLLATE utf8mb4_0900_ai_ci;

/* create management tables (should be in the same database to easily use trandactions) */
CREATE TABLE IF NOT EXISTS `dsm-datasets`.`_dsm_datasets` (
  `name` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`name`) ,
  `schema` TEXT NOT NULL ,
  `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` BOOLEAN NOT NULL DEFAULT FALSE,
  `deletedAt` TIMESTAMP NULL DEFAULT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `dsm-datasets`.`_dsm_repos` (
  `name` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`name`) ,
  FOREIGN KEY (`name`) REFERENCES `dsm-datasets`.`_dsm_datasets`(`name`) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `dsm-datasets`.`_dsm_views` (
  `name` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`name`) ,
  FOREIGN KEY (`name`) REFERENCES `dsm-datasets`.`_dsm_datasets`(`name`) ON DELETE CASCADE ,
  `base` VARCHAR(256) NOT NULL ,
  FOREIGN KEY (`base`) REFERENCES `dsm-datasets`.`_dsm_datasets`(`name`) ON DELETE RESTRICT ,
  `fields` TEXT NOT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `dsm-datasets`.`_dsm_refs` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  `origin` VARCHAR(256) NOT NULL ,
  FOREIGN KEY (`origin`) REFERENCES `dsm-datasets`.`_dsm_datasets`(`name`) ON DELETE CASCADE ,
  `target` VARCHAR(256) NOT NULL ,
  FOREIGN KEY (`target`) REFERENCES `dsm-datasets`.`_dsm_datasets`(`name`) ON DELETE RESTRICT ,
  INDEX (`origin`, `target`)
) ENGINE = InnoDB;

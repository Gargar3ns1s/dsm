# dsm
dataset management system

## git workflow
This project uses GitHub flow (dedscribed here: https://docs.github.com/en/get-started/quickstart/github-flow). A Good visualization of the branching strategy can be found here: https://www.gitkraken.com/learn/git/best-practices/git-branch-strategy#github-flow-branch-strategy

All binary files should be handled by `git lfs`. Git is great at tracking text changes but doesn't handle binary data very well. That's why we want lfs (large file storage) to take care of binary data.

## Development setup
### on macOS
make sure homebrew is available on your system.

#### install nvm (node version manager)
Install instructions here: https://github.com/nvm-sh/nvm#installing-and-updating

`cd` into the repository and run `nvm install` to install the appropriate nodejs version for the project

#### install git-lfs

```bash
brew install git-lfs
git lfs install
```

#### set up kubernetes (k8s) cluster and tools:

Install docker desktop (https://www.docker.com/products/docker-desktop)

Once installed enable kubernetes (https://docs.docker.com/desktop/kubernetes/#enable-kubernetes)

Install tilt (integrated development environment for k8s) and kubectl:

```bash
brew install tilt kubernetes-cli
```

### When starting to develop

make sure kubernetes is running

`cd` into the repository

run:

```bash
nvm use # reads node version from .nvmrc file
tilt up # let the command run in a seperate terminal window
```

The tilt dashboard offers a nice overview of the microservices deployment (http://localhost:10350/). It allows you to access individual api's for dev.

The app itself is hosted at http://localhost:3600/ with ingress routing applied.


## only if you run this for the frist time
1. `cd` /deployment/dev
2. `kubectl apply -k ./` to asign the namespace
3. open phpMyAdmin http://localhost:3802 with credentials root/pass
4. Execute the SQL from Bootstrap.sql


## useful VSCode extensions:
- "GitLens - Git supercharged"
- "TypeScipt Hero"
- "Docker"
- "tilt" (tiltfile language support fro VSCode)

## Swagger
Swagger is hostet at http://localhost:3601/api/swagger